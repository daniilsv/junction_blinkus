﻿
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreCanvasScript : MonoBehaviour {
    private Text textField;
    private int score = 0;

    public Camera cam;

    void Awake() {
        textField = GetComponent<Text>();
    }

    void Start() {
        if (cam == null) {
            cam = Camera.main;
        }

        if (cam != null) {
            // Tie this to the camera, and do not keep the local orientation.
            transform.SetParent(cam.GetComponent<Transform>(), true);
        }
    }
    public void ResetScore() {
        score = 0;
    }
    public void AddScore(int sc) {
        score += sc;
    }
    void LateUpdate() {
        score = (int)Camera.main.transform.position.z;
        textField.text = string.Format("{0}", score);
    }
}
