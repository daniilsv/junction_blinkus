﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class EveryTimer : MonoBehaviour {

    void Start() {
        foreach (Camera c in Camera.allCameras) {
            BloomOptimized bo = c.gameObject.AddComponent<BloomOptimized>();
            bo.intensity = 2;
            bo.fastBloomShader = Shader.Find("Hidden/FastBloom");
        }
        InvokeRepeating("LaunchEnemies", 2.0f, 0.5f);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Q)) {
            GameObject bul = Instantiate(GameObject.Find("SphereForClone"), transform.position, new Quaternion()) as GameObject;
            bul.GetComponent<Bullet>().setVel(Camera.main.transform.forward);
        }
        if (Input.GetKeyDown(KeyCode.W)) {
            GameObject bul = Instantiate(GameObject.Find("LaserForClone")) as GameObject;
            bul.transform.position = transform.position;
            bul.transform.rotation = Camera.main.transform.rotation;
            bul.GetComponent<Laser>().setVel(Camera.main.transform.forward);
        }
        Camera.main.transform.position += Camera.main.transform.forward * 0.05f;
    }
    public void LaunchEnemies() {
        Vector3 position = transform.position + new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), 30);
        GameObject enm = Instantiate(GameObject.Find("EnemyForClone"), position, new Quaternion()) as GameObject;
        enm.GetComponent<Renderer>().material.mainTexture = (Texture)Resources.Load("hf"+Random.Range(0,5));
        enm.GetComponent<Enemy>().setVel(new Vector3(0, 0, -1));
    }
}
