﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    private Vector3 mVel = Vector3.zero;

    void Start() {
    }

    public void setVel(Vector3 vel) {
        mVel = vel * 7;
    }

    void Update() {

        if (GetComponent<Collider>().isTrigger) {
            Destroy(gameObject);
        }
        transform.position += mVel * Time.deltaTime;
        if ((transform.position - Camera.main.transform.position).magnitude > 40)
            Destroy(gameObject);
    }

}
