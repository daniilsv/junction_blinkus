﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    private Vector3 mVel = Vector3.zero;

    void Start() {

    }
    public void setVel(Vector3 vel) {
        mVel = vel * 2.5f;
    }

    void Update() {
        transform.position += mVel * Time.deltaTime;
        transform.Rotate(new Vector3(1, 0, 1));
        if (transform.position.z - Camera.main.transform.position.z < -20)
            Destroy(gameObject);
    }
    void OnTriggerEnter(Collider other) {
        if (other.Equals(Camera.main.GetComponent<Collider>())) {
            Debug.Log("END");
        }
        if (gameObject.name.Contains("(Clone)")) {
            Destroy(gameObject);
        }
    }
}
