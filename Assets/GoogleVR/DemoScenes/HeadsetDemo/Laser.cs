﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {
    private Vector3 mVel = Vector3.zero;

    void Create() {
    }

    public void setVel(Vector3 vel) {
        mVel = vel * 13;
    }

    void Update() {
        if (GetComponent<Collider>().isTrigger) {
            Destroy(gameObject);
        }
        transform.position += mVel * Time.deltaTime;
        if ((transform.position - Camera.main.transform.position).magnitude > 40)
            Destroy(gameObject);
    }
   
}